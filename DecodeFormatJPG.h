#pragma once

#include "Decode.h"
#include "Properties.h"

#include <KIR\KIR4_gdi_bitmap.h>
#include <KIR\KIR4_console.h>

#define CodeFormatJPG 12

class DecodeFormatJPG:public Decode
{
	unsigned
		size;
	KIR4::gdi_bitmap BitmapCreate(const char *data)
	{
		KIR4::gdi_bitmap
			bitmap;
		bitmap.buffer_load((const unsigned char*)data, size);

		return bitmap;
	}
public:
	DecodeFormatJPG(unsigned size):
		size(size)
	{
	}
	virtual ~DecodeFormatJPG()
	{
	}
	virtual bool Save(const char *data, const std::wstring &original, const std::wstring &resized, int w, int h, Gdiplus::Color color)
	{
		KIR4::gdi_bitmap
			bitmap = BitmapCreate(data);
		return SaveBitmap(bitmap, original, resized, w, h, color, L"image/jpeg");
	}
	virtual void Print()
	{
		clog.color(KIR4::LPURPLE) << "DecodeFormatJPG" << KIR4::eol;
		clog.color(KIR4::LPURPLE) << "size: ";
		clog.color(KIR4::PURPLE) << size << KIR4::eol;
	}
};