#pragma once

#include "Decode.h"
#include "Properties.h"

#include <KIR\KIR4_gdi_bitmap.h>
#include <KIR\KIR4_console.h>

#define CodeFormatRGBA 10

class DecodeFormatRGBA:public Decode
{
	int
		width = 0,
		height = 0;
	KIR4::gdi_bitmap BitmapCreate(const char *data)
	{
		KIR4::gdi_bitmap
			bitmap = new Gdiplus::Bitmap(width, height);

		KIR4::gdi_bitmap::lock_BGRA
			lock_bgr;

		lock_bgr.lock(bitmap);

		for (int y = 0; y < height; ++y)
			CopyMemory(lock_bgr.pixels() + y*lock_bgr.pitch(), data + y*lock_bgr.row_pixel_bytes(), lock_bgr.row_pixel_bytes());

		lock_bgr.unlock();

		return bitmap;
	}
public:
	DecodeFormatRGBA(int width, int height):
		width(width),
		height(height)
	{
	}
	virtual ~DecodeFormatRGBA()
	{
	}
	virtual bool Save(const char *data, const std::wstring &original, const std::wstring &resized, int w, int h, Gdiplus::Color color)
	{
		KIR4::gdi_bitmap
			bitmap = BitmapCreate(data);
		return SaveBitmap(bitmap, original, resized, w, h, color);
	}
	virtual void Print()
	{
		clog.color(KIR4::LPURPLE) << "DecodeFormatRGBA" << KIR4::eol;
		clog.color(KIR4::LPURPLE) << "size: ";
		clog.color(KIR4::PURPLE) << width;
		clog.color(KIR4::LPURPLE) << " * ";
		clog.color(KIR4::PURPLE) << height << KIR4::eol;
	}
};