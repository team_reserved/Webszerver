#pragma once

#define PLUG_SOCKET_STATUS_ERROR 9
#define PLUG_SOCKET_STATUS_WAIT_FOR_HANDSHAKE 10
#define PLUG_SOCKET_STATUS_WAIT_FOR_FORMAT 11
#define PLUG_SOCKET_STATUS_WAIT_FOR_DATA 12

#include "PlugSocket.h"
#include "Properties.h"

class ServerEvent
{
	bool
		done;
	enum
	{
		EV_REMOVE_CLIENT,
		EV_HELP,
		EV_SERVER_INFORMATIONS,
		EV_START,
		EV_STOP,
		EV_EXIT,
		EV_CLIENTS_INFORMATIONS,
		EV_SET_SAVE_DIR,
		EV_SET_PORT,
		EV_SET_RESIZED_IMAGE_SIZE,
		EV_SET_RESIZED_IMAGE_BACKGROUND_COLOR,
		EV_SAVE,
		EV_SAVE_PROPERTIES,
		EV_RESTORE_DEFAULTS,
		EV_IMAGES_LINK,
		EV_RESTART,
	};
	KIR4::serverTCP_list<PlugSocket>
		server;
	Properties
		properties;
	unsigned GetFreeID()
	{
		unsigned
			ID = 0;

		auto
			it = server.get_clients().begin();
		while (it != server.get_clients().end())
		{
			if ((*it)->GetID() == ID)
			{
				ID++;
				it = server.get_clients().begin();
			}
			else
				it++;
		}

		return ID;
	}
	bool RemoveUser(unsigned ID)
	{
		auto
			it = server.get_clients().begin();
		for (it; it != server.get_clients().end(); it++)
			if ((*it)->GetID() == ID)
			{
				(*it)->kill();
				//delete *it;   tooo hard remove
				//server.get_clients().erase(it);
				return true;
			}
		return false;
	}
public:
	ServerEvent()
	{
		properties.os << "Program started at " << KIR4::time().str() << KIR4::eol;
		properties.os << "Program version " << PROGRAM_VERSION << KIR4::eol;

		if (properties.CheckImagesDirectory())
		{
			if (int e = CreateDirectoryW(properties.GetImgsDir(), 0))
			{
				clog.color(KIR4::LRED) << "Directory create error: " << properties.GetImgsDir() << "  return: " << e << KIR4::eol;
				properties.os << "Directory create error: " << properties.GetImgsDir() << "  return: " << e << KIR4::eol;
			}
			else
			{
				properties.os << "Directory created: " << properties.GetImgsDir() << KIR4::eol;
			}
		}
		properties.WriteLog();

		clog.add("rm user ", "<ID>", EV_REMOVE_CLIENT);
		clog.add("help", "", EV_HELP);
		clog.add("server info", "", EV_SERVER_INFORMATIONS);
		clog.add("start", "", EV_START);
		clog.add("stop", "", EV_STOP);
		clog.add("exit", "", EV_EXIT);
		clog.add("restart", "", EV_RESTART);
		clog.add("save", "", EV_SAVE_PROPERTIES);
		clog.add("restore defaults", "", EV_RESTORE_DEFAULTS);
		clog.add("users info", "", EV_CLIENTS_INFORMATIONS);
		clog.add("save dir ", "<path>", EV_SET_SAVE_DIR);
		clog.add("port ", "<port>", EV_SET_PORT);
		clog.add("resize image ", "<width>*<height>", EV_SET_RESIZED_IMAGE_SIZE);
		clog.add("resized image background ", "<r>,<g>,<b>,<a>", EV_SET_RESIZED_IMAGE_BACKGROUND_COLOR);
		clog.add("link ", "<link>", EV_IMAGES_LINK);


		KIR4::char_string
			commands(properties.GetExecuteingCode());

		if (commands.length())
			for (auto it : commands.split_lines())
				if (it.length())
					clog.execute(it);
	}
	~ServerEvent()
	{
		properties.os << "Program stopped at " << KIR4::time().str() << KIR4::eol;

		properties.WriteLog();
	}
	void _EV_SAVE_PROPERTIES(const KIR4::wchar_t_string &args)
	{
		properties.Save();
	}
	void _EV_RESTORE_DEFAULTS(const KIR4::wchar_t_string &args)
	{
		properties.RestoreDefaults();
	}
	void _EV_SET_RESIZED_IMAGE_SIZE(const KIR4::wchar_t_string &args)
	{
		if (!properties.SetResizeSize(args))
			clog.color(KIR4::LRED) << "Invalid size format (<width>*<height>)!" << KIR4::eol;
	}
	void _EV_SET_RESIZED_IMAGE_BACKGROUND_COLOR(const KIR4::wchar_t_string &args)
	{
		if (!properties.SetResizeColor(args))
			clog.color(KIR4::LRED) << "Invalid color format (<r>,<g>,<b>,<a>)!" << KIR4::eol;
	}
	void _EV_REMOVE_CLIENT(const KIR4::wchar_t_string &args)
	{
		unsigned
			id = KIR4::to_int(args);
		if (!RemoveUser(id))
		{
			clog.color(KIR4::LRED) << "User not found: " << id << KIR4::eol;
			clog.color();
		}
		else
		{
			clog.color(KIR4::GREEN) << "User removed: " << id << KIR4::eol;
			clog.color();
		}
	}
	void _EV_SAVE(const KIR4::wchar_t_string &args)
	{
		clog.color(KIR4::LYELLOW) << "Properties save... ";
		properties.Save();
		clog.color(KIR4::LYELLOW) << "DONE!" << KIR4::eol;
	}
	void _EV_SET_SAVE_DIR(const KIR4::wchar_t_string &args)
	{
		properties.SetImgsDir(args.c_str());
	}
	void _EV_SET_PORT(const KIR4::wchar_t_string &args)
	{
		if (args.length())
		{
			int
				port = KIR4::to_int(args);
			if (port < 1024)
			{
				clog.color(KIR4::LYELLOW) << "WARNING! port value is under 1024" << KIR4::eol << server.error_list;
				clog.color();
			}
			properties.SetPort(KIR4::to_wstring(port).c_str());
		}
		else
		{
			clog.color(KIR4::LRED) << "Entered port is too short." << KIR4::eol << server.error_list;
			clog.color();
		}
	}
	void _EV_HELP(const KIR4::wchar_t_string &args)
	{
		clog << clog;
	}
	void _EV_SERVER_INFORMATIONS(const KIR4::wchar_t_string &args)
	{
		(clog.color(KIR4::LYELLOW) << "Version: ").color(KIR4::YELLOW) << PROGRAM_VERSION << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Build date: ").color(KIR4::YELLOW) << __DATE__ << ' ' << __TIME__ << KIR4::eol;
		clog << KIR4::YELLOW << "Compatible client versions: " << KIR4::LYELLOW;
		properties.PrintCompatibleVersions();
		clog.color(KIR4::LYELLOW) << "Server ip and port: " << KIR4::eol;
		clog.color(KIR4::LYELLOW) << "- - - - -" << KIR4::eol;
		clog.color(KIR4::YELLOW) << server.get_ip_port();
		clog.color(KIR4::LYELLOW) << "- - - - -" << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Status: ").color(KIR4::YELLOW) << server.server_status << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Started time: ").color(KIR4::YELLOW) << server.server_started_time << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Number of connected users: ").color(KIR4::YELLOW) << server.get_clients().size() << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Received bytes: ").color(KIR4::YELLOW) << server.get_recv_bytes() << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Sent bytes: ").color(KIR4::YELLOW) << server.get_send_bytes() << KIR4::eol;
		properties.Print();
		clog.color();
	}
	void _EV_START(const KIR4::wchar_t_string &args)
	{
		clog.color(KIR4::LYELLOW) << "Server start... ";
		if (server.start(KIR4::char_string(properties.GetPort()).c_str()))
		{
			clog.color(KIR4::LYELLOW) << "DONE!" << KIR4::eol;
		}
		else
		{
			clog.color(KIR4::LRED) << "ERROR:\n" << server.error_list;
			clog.color();
		}
	}
	void _EV_RESTART(const KIR4::wchar_t_string &args)
	{
		_EV_STOP(args);
		_EV_START(args);
	}
	void _EV_STOP(const KIR4::wchar_t_string &args)
	{
		if (server.is_running())
		{
			clog.color(KIR4::LYELLOW) << "Server shut down... ";
			server.stop();
			clog.color(KIR4::LYELLOW) << "DONE!" << KIR4::eol;
		}
		else
		{
			clog.color(KIR4::LYELLOW) << "Server is stopped." << KIR4::eol;
			clog.color();
		}
	}
	void _EV_CLIENTS_INFORMATIONS(const KIR4::wchar_t_string &args)
	{
		for (auto it : server.get_clients())
		{
			it->Print();
			it++;
		}
		clog.color(KIR4::YELLOW) << "Total connected users: " << server.get_clients().size() << KIR4::eol;
		clog.color();
	}
	void _EV_IMAGES_LINK(const KIR4::wchar_t_string &args)
	{
		properties.SetImgsLink(args.c_str());
	}
	void _EV_EXIT(const KIR4::wchar_t_string &args)
	{
		done = true;
	}
	void CommandExecute(int ID, const KIR4::wchar_t_string &args)
	{
		switch (ID)
		{
			case EV_SAVE_PROPERTIES:
			{
				_EV_SAVE_PROPERTIES(args);
				break;
			}
			case EV_RESTORE_DEFAULTS:
			{
				_EV_RESTORE_DEFAULTS(args);
				break;
			}
			case EV_SET_RESIZED_IMAGE_SIZE:
			{
				_EV_SET_RESIZED_IMAGE_SIZE(args);
				break;
			}
			case EV_SET_RESIZED_IMAGE_BACKGROUND_COLOR:
			{
				_EV_SET_RESIZED_IMAGE_BACKGROUND_COLOR(args);
				break;
			}
			case EV_REMOVE_CLIENT:
			{
				_EV_REMOVE_CLIENT(args);
				break;
			}
			case EV_SAVE:
			{
				_EV_SAVE(args);
				break;
			}
			case EV_SET_SAVE_DIR:
			{
				_EV_SET_SAVE_DIR(args);
				break;
			}
			case EV_SET_PORT:
			{
				_EV_SET_PORT(args);
				break;
			}
			case EV_HELP:
			{
				_EV_HELP(args);
				break;
			}
			case EV_SERVER_INFORMATIONS:
			{
				_EV_SERVER_INFORMATIONS(args);
				break;
			}
			case EV_START:
			{
				_EV_START(args);
				break;
			}
			case EV_EXIT:
			{
				_EV_EXIT(args);
				break;
			}
			case EV_RESTART:
			{
				_EV_RESTART(args);
				break;
			}
			case EV_STOP:
			{
				_EV_STOP(args);
				break;
			}
			case EV_CLIENTS_INFORMATIONS:
			{
				_EV_CLIENTS_INFORMATIONS(args);
				break;
			}
			case EV_IMAGES_LINK:
			{
				_EV_IMAGES_LINK(args);
				break;
			}
		}
	}
	void run()
	{
		done = false;
		while (!done)
		{
			clog.EVENT_TIMER();
			if (clog.executable())
				CommandExecute(clog.ID(), KIR4::wchar_t_string(clog.arguments()));
			if (server.is_running())
			{
				auto
					it = server.get_clients().begin();
				PlugSocket
					*current;
				server.accept_incoming_client();
				if (it != server.get_clients().end())
				{
					do
					{
						current = *it;
						it++;
						if (current->get_status() == PLUG_SOCKET_STATUS_WAIT)
						{
							current->Set(GetFreeID(), &properties);
							clog.color(KIR4::LPURPLE) << "[ " << KIR4::time().str() << " ]";
							(clog.color(KIR4::GREEN) << "  User connected, ID: ").color(KIR4::LGREEN) << current->GetID() << KIR4::eol;
							(clog.color(KIR4::GREEN) << "IP address: ").color(KIR4::LGREEN) << current->get_ip_port() << KIR4::eol;
							properties.os << "User connected, ID: " << current->GetID() << "\tIP address: " << current->get_ip_port().str() << KIR4::eol;

							current->accept();
							if (!current->start())
							{
								current->kill();
								(clog.color(KIR4::RED) << "User start error, ID: ").color(KIR4::LRED) << current->GetID() << KIR4::eol;
								properties.os << "User start error, ID: " << current->GetID() << KIR4::eol;
							}
							else
								current->set_status(PLUG_SOCKET_STATUS_WAIT_FOR_HANDSHAKE);

							properties.WriteLog();
						}
						if (current->get_status() == PLUG_SOCKET_STATUS_WAIT_FOR_HANDSHAKE)
						{
							KIR4::socket_message
								*message = current->get_recv_message(CodeHandshakeRequest);

							if (message)
							{
								auto
									username = message->get_swstr();
								auto
									version = message->get_swstr();

								if (username)
								{
									if (properties.IsCompatibleVersion(version))
									{
										current->SetUser(username, version);
										if (!current->CheckFileStrucure())
										{
											KIR4::socket_message
												*message = new KIR4::socket_message_dynamic(CodeHandshakeRefused);
											message->set_str("Invalid file structure (maybe you are not registered).");
											current->send_message(message);
											current->set_status(PLUG_SOCKET_STATUS_ERROR);
										}
										else
										{
											clog << "CodeHandshakeSuccess" << KIR4::eol;
											KIR4::socket_message
												*message = new KIR4::socket_message_dynamic(CodeHandshakeSuccess);
											message->set_wstr(PROGRAM_VERSION);
											current->send_message(message);
											current->set_status(PLUG_SOCKET_STATUS_WAIT_FOR_FORMAT);
										}
									}
									else
									{
										clog << KIR4::RED << "Not compatible version: " << KIR4::LRED << version << KIR4::eol;
										KIR4::socket_message
											*message = new KIR4::socket_message_dynamic(CodeHandshakeRefused);
										message->set_str("Not compatible version");
										current->send_message(message);
										current->set_status(PLUG_SOCKET_STATUS_ERROR);
									}
								}
								else
								{
									clog << KIR4::RED << "Invalid username: " << KIR4::LRED << username << KIR4::eol;
									KIR4::socket_message
										*message = new KIR4::socket_message_dynamic(CodeHandshakeRefused);
									message->set_str("Invalid username.");
									current->send_message(message);
									current->set_status(PLUG_SOCKET_STATUS_ERROR);
								}
								message->proccessed();
							}
						}
						else if (current->get_status() == PLUG_SOCKET_STATUS_WAIT_FOR_FORMAT)
						{
							current->RecvFormat();
						}
						else if (current->get_status() == PLUG_SOCKET_STATUS_WAIT_FOR_DATA)
						{
							current->RecvData();
						}
						if (current->is_error())
						{
							(clog.color(KIR4::RED) << "User disconnected, ID: ").color(KIR4::LRED) << current->GetID() << KIR4::eol;
							clog.color(KIR4::RED) << "\tconnected time: ";
							clog.color(KIR4::LRED) << current->get_started_time().str("%Y.%m.%d. %H:%M:%S");
							clog.color(KIR4::RED) << "\t  (it was ";
							clog.color(KIR4::LRED) << (KIR4::time() - current->get_started_time()).to_time_t() / 60;
							clog.color(KIR4::RED) << " seconds ago )" << KIR4::eol;
							clog.color(KIR4::RED) << "\t sent bytes: ";
							unsigned long long
								sent_bytes = current->GetSentBytes();
							unsigned long long
								recv_bytes = current->GetReceivedBytes();
							clog.color(KIR4::LRED) << sent_bytes << "\t" << sent_bytes / double(1024 * 1024) << " MB" << KIR4::eol;
							clog.color(KIR4::RED) << "\t reveived bytes: ";
							clog.color(KIR4::LRED) << recv_bytes << "\t" << recv_bytes / double(1024 * 1024) << " MB" << KIR4::eol;

							properties.os << "User disconnected, ID: " << current->GetID() << "\t";
							properties.os << "\tconnected time: ";
							properties.os << current->get_started_time().str("%Y.%m.%d. %H:%M:%S");
							properties.os << "\t  (it was ";
							properties.os << (KIR4::time() - current->get_started_time()).to_time_t();
							properties.os << " times ago )";
							properties.os << "\t sent bytes: ";
							properties.os << current->GetSentBytes();
							properties.os << "\t reveived bytes: ";
							properties.os << current->GetReceivedBytes() << KIR4::eol;

							properties.WriteLog();
							auto
								rm = it;
							rm--;
							server.get_clients().erase(rm);
							delete current;
							continue;
						}

						//auto
						//	it = current->receive_begin();
						//while (it != current->receive_end())
						//{
						//	(**it).proccessed();
						//	it++;
						//}
						current->recv_message_collector();
					}
					while (it != server.get_clients().end());
				}
			}
			Sleep(300);
		}
	}
};