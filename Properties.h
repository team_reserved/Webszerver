#pragma once

#include <string>
#include <fstream>

#include <KIR\KIR4_gdi_bitmap.h>

const wchar_t LogFilename[] = L"ServerLog.txt";

const wchar_t FileNameCodes[] = {
	L'0',L'1',L'2',L'3',L'4',L'5',L'6',L'7',L'8',L'9',

	L'q',L'w',L'e',L'r',L't',L'z',L'u',L'i',L'o',L'p',L'a',L's',L'd',L'f',L'g',
	L'h',L'j',L'k',L'l',L'y',L'x',L'c',L'v',L'b',L'n',L'm',

	L'Q',L'W',L'E',L'R',L'T',L'Z',L'U',L'I',L'O',L'P',L'A',L'S',L'D',L'F',L'G',
	L'H',L'J',L'K',L'L',L'Y',L'X',L'C',L'V',L'B',L'N',L'M'
};

const wchar_t ORIGINAL[] = L"original";
const wchar_t RESIZED[] = L"resized";

class Properties
{
	std::list<std::wstring>
		comaptibleversions;
	std::wstring
		imageslink = L"",
		imagesdirectory = L"users\\",
		port = L"4356",
		executeingcode = L"start\n";
	unsigned
		resizewidth = 100,
		resizeheight = 100;
	Gdiplus::Color
		resizecolor = Gdiplus::Color(0, 0, 0, 0);
	inline void EndOfPath(std::wstring &str)
	{
		if (str.length())
		{
			if (str[str.length() - 1] != L'\\' && str[str.length() - 1] != L'/')
				str += L'\\';
		}
	}
	inline void ErrorCheckImagesDirectory() const
	{
		if (CheckImagesDirectory())
			(clog.color(KIR4::LGREEN) << "Main directory OK: ").color(KIR4::GREEN) << imagesdirectory << KIR4::eol;
		else
			(clog.color(KIR4::LRED) << "Main directory ERROR: ").color(KIR4::RED) << imagesdirectory << KIR4::eol;
	}
	std::ofstream
		log;
public:
	std::ostringstream
		os;
	bool WriteLog()
	{
		log.open(LogFilename, std::ios::app);
		if (log.is_open())
		{
			log << os.str();
			log.close();
			os.clear();
			return true;
		}
		else
			return false;
	}
	Properties()
	{
		Load();
	}
	~Properties()
	{
		Save();
	}
	void AddCompatibleVersion(const wchar_t *version)
	{
		if (version)
			comaptibleversions.push_back(version);
	}
	//true visszatérés ha szerepelt a listába, false ha nem
	bool RemoveCompatibleVersion(const wchar_t *version)
	{
		if (version)
		{
			auto
				it = comaptibleversions.begin();
			while (it != comaptibleversions.end())
			{
				if (*it == version)
				{
					comaptibleversions.erase(it);
					return true;
				}
				it++;
			}
		}
		return false;
	}
	void DefaultCompatibleVersion()
	{
		comaptibleversions.clear();
		for (int i = 0; i < sizeof(COMPATIBLE_VERSIONS) / sizeof(const char *); i++)
			comaptibleversions.push_back(COMPATIBLE_VERSIONS[i]);
	}
	bool IsCompatibleVersion(const wchar_t *version)
	{
		if (version)
			for (auto &it : comaptibleversions)
				if (it == version)
					return true;
		return false;
	}
	void PrintCompatibleVersions()
	{
		clog << KIR4::LYELLOW << " { ";
		for (auto &it : comaptibleversions)
			clog << KIR4::YELLOW << it << KIR4::LYELLOW << ", ";
		clog << "}" << KIR4::eol;
	}
	inline std::wstring GetDirectory(const std::wstring &user, const wchar_t *type/*original or resized*/) const
	{
		return imagesdirectory + user + L"\\" + type + L"\\";
	}
	std::wstring GetNextFilename(const std::wstring &directory, const std::wstring &extension = L"png") const
	{
		KIR4::find_files_w
			ff(directory.c_str());
		unsigned
			max = 0,
			current;

		for (auto it : ff)
			if (it.extension() == extension)
			{
				current = KIR4::to_unsigned_int(it.name().substr(0, 10));
				if (current > max)
					max = current;
			}

		std::wstring
			filecode = L"0000000000000000000000",
			filename = L"0000000000",
			number = KIR4::to_wstring(max + 1);

		for (unsigned i = 0; i < number.length(); i++)
			filename[i + filename.length() - number.length()] = number[i];

		for (unsigned i = 0; i < filecode.length(); i++)
			filecode[i] = FileNameCodes[(rand() % (sizeof(FileNameCodes) / sizeof(wchar_t)))];

		return filename + filecode + L"." + extension;
	}
	void RestoreDefaults()
	{
		imagesdirectory = L"users\\";
		port = L"4356";
		executeingcode = L"start\n";

		resizewidth = 100;
		resizeheight = 100;

		resizecolor = Gdiplus::Color(0, 0, 0, 0);

		DefaultCompatibleVersion();
	}
	void SetImgsLink(const wchar_t *link)
	{
		imageslink = link;
		EndOfPath(imageslink);
		clog.color(KIR4::LYELLOW) << "Images link has been changed: " << this->imageslink << ORIGINAL << "//*" << KIR4::eol;
		clog.color();
	}
	void SetImgsDir(const wchar_t *dir)
	{
		imagesdirectory = dir;
		EndOfPath(imagesdirectory);
		clog.color(KIR4::LYELLOW) << "Images directory has been changed: " << this->imagesdirectory << KIR4::eol;
		clog.color();
		ErrorCheckImagesDirectory();
	}
	void SetPort(const wchar_t *port)
	{
		this->port = port;
		clog.color(KIR4::LYELLOW) << "Port has been changed: " << this->port << KIR4::eol;
		clog.color();
	}
	bool SetResizeColor(KIR4::wchar_t_string str)
	{
		str = ',' + str + ',';
		auto
			rgba_list = str.split(',', ',');
		if (rgba_list.size() >= 4)
		{
			auto
				rgba = rgba_list.begin();
			int r = KIR4::to_int((std::wstring)*rgba);
			rgba++;
			int g = KIR4::to_int((std::wstring)*rgba);
			rgba++;
			int b = KIR4::to_int((std::wstring)*rgba);
			rgba++;
			int a = KIR4::to_int((std::wstring)*rgba);

			resizecolor = Gdiplus::Color(r, g, b, a);
			return true;
		}
		return false;
	}
	bool SetResizeSize(KIR4::wchar_t_string str)
	{
		str = '*' + str + '*';
		auto
			size_list = str.split('*', '*');
		if (size_list.size() >= 2)
		{
			auto
				size = size_list.begin();
			resizewidth = KIR4::to_unsigned_int((std::wstring)*size);
			size++;
			resizeheight = KIR4::to_unsigned_int((std::wstring)*size);
			return true;
		}
		return false;
	}
	inline void SetResizeColor(Gdiplus::Color color)
	{
		resizecolor = color;
	}
	inline void SetResizeWidth(unsigned width)
	{
		resizewidth = width;
	}
	inline void SetResizeHeight(unsigned height)
	{
		resizeheight = height;
	}

	inline Gdiplus::Color GetResizeColor() const
	{
		return resizecolor;
	}
	inline unsigned GetResizeWidth() const
	{
		return resizewidth;
	}
	inline unsigned GetResizeHeight() const
	{
		return resizeheight;
	}
	inline const wchar_t *GetImgsLink() const
	{
		return imageslink.c_str();
	}
	inline const wchar_t *GetImgsDir() const
	{
		return imagesdirectory.substr(imagesdirectory.length() - 1).c_str();
	}
	inline const wchar_t *GetPort() const
	{
		return port.c_str();
	}
	inline const wchar_t *GetExecuteingCode() const
	{
		return executeingcode.c_str();
	}
	void Load()
	{
		std::ifstream
			file("properties.dat");

		std::ostringstream
			os;
		if (file.is_open())
			os << file.rdbuf();
		file.close();

		KIR4::wchar_t_string
			data(os.str());

		if (data.length())
		{
			KIR4::wchar_t_string
				result;
			unsigned
				pos;

			if (data.get(result, pos = 0, "Resized Images Size: ", "\n", false))
			{
				if (!SetResizeSize(result))
					clog.color(KIR4::LRED) << "PROPERTIES: Invalid size format! Defaults are loaded." << KIR4::eol;
			}
			else
				clog.color(KIR4::LRED) << "PROPERTIES: size not found." << KIR4::eol;
			if (data.get(result, pos = 0, "Resized Images Background Color: ", "\n", false))
			{
				if (!SetResizeColor(result))
					clog.color(KIR4::LRED) << "PROPERTIES: Invalid color format! Defaults are loaded." << KIR4::eol;
			}
			else
				clog.color(KIR4::LRED) << "PROPERTIES: color not found." << KIR4::eol;
			if (data.get(result, pos = 0, "Images Directory: ", "\n", false))
				imagesdirectory = result.str();
			else
				clog.color(KIR4::LRED) << "PROPERTIES: directory not found." << KIR4::eol;

			if (data.get(result, pos = 0, "Server Port: ", "\n", false))
				port = result.str();
			else
				clog.color(KIR4::LRED) << "PROPERTIES: port not found." << KIR4::eol;
			if (data.get(result, pos = 0, "Executeing code START\n", "Executeing code END", false))
				executeingcode = result.str();
			else
				clog.color(KIR4::LRED) << "PROPERTIES: code not found." << KIR4::eol;
			if (data.get(result, pos = 0, "Images Link: ", "\n", false))
				imageslink = result.str();
			else
				clog.color(KIR4::LRED) << "PROPERTIES: link not found." << KIR4::eol;
			EndOfPath(imagesdirectory);
			EndOfPath(imageslink);

			if (data.get(result, pos = 0, "Images Link: ", "\n", false))
				imageslink = result.str();
			else
				clog.color(KIR4::LRED) << "PROPERTIES: link not found." << KIR4::eol;

			if (data.get(result, pos = 0, "CompatibleVersions {", "}", false))
			{
				auto
					list = result.split(',', ',');
				for (auto &it : list)
					AddCompatibleVersion(it.c_str());
			}
			else
			{
				clog.color(KIR4::LRED) << "PROPERTIES: compatible versions not found" << KIR4::LYELLOW << " do you want to restore default? " << KIR4::LWHITE << "[y/n]: " << KIR4::YELLOW;
				if (clog.fi())
					DefaultCompatibleVersion();
			}
		}
	}
	inline bool CheckDirectory(const wchar_t *dir) const
	{
		return GetFileAttributesW(dir) == FILE_ATTRIBUTE_DIRECTORY;
	}
	inline bool CheckImagesDirectory() const
	{
		return CheckDirectory(GetImgsDir());
	}
	void Save() const
	{
		std::wofstream
			file("properties.dat");

		if (file.is_open())
		{
			file << "Resized Images Size: " << resizewidth << '*' << resizeheight << "\n";
			file << "Resized Images Background Color: " << int(resizecolor.GetR()) << ',' << int(resizecolor.GetG()) << ',' << int(resizecolor.GetB()) << ',' << int(resizecolor.GetA()) << "\n";
			file << "Images Directory: " << imagesdirectory << "\n";
			file << "Images Link: " << imageslink << "\n";
			file << "Server Port: " << port << "\n";
			file << "Executeing code START\n" << executeingcode << "Executeing code END\n";
			file << "CompatibleVersions {,";
			for (auto &it : comaptibleversions)
				file << it << ",";
			file << "}\n";
		}
		file.close();
	}
	void Print()
	{
		clog.color(KIR4::YELLOW) << "Properties:\n" << KIR4::eol;
		clog.color(KIR4::LYELLOW) << "Images Directory: " << imagesdirectory << KIR4::eol;
		clog.color(KIR4::LYELLOW) << "Images Link: " << imageslink << KIR4::eol;
		clog.color(KIR4::LYELLOW) << "Server Port: " << port << KIR4::eol;
		clog.color(KIR4::LYELLOW) << "Executeing code:\n---\n" << executeingcode << "---" << KIR4::eol;
		clog.color(KIR4::LYELLOW) << "Resized Images Size: " << resizewidth << '*' << resizeheight << KIR4::eol;
		clog.color(KIR4::LYELLOW) << "Resized Images Background Color: " << int(resizecolor.GetR()) << ',' << int(resizecolor.GetG()) << ',' << int(resizecolor.GetB()) << ',' << int(resizecolor.GetA()) << KIR4::eol;
	}
};