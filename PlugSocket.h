#pragma once

#include <KIR\KIR4_system.h>

#include "Properties.h"
#include "Decode.h"
//#include "BitmapUpload.h"
//#include "FileUpload.h"

#include "DecodeFormatRGB.h"
#include "DecodeFormatRGBA.h"
#include "DecodeFormatPNG.h"
#include "DecodeFormatJPG.h"

#define CodeHandshakeRefused 97
#define CodeHandshakeSuccess 98
#define CodeHandshakeRequest 99
#define CodeEndOfData 100
#define CodeFormatAccepted 101
#define CodeFormatRefused 102
#define CodeData 103
#define CodeDataError 104

class PlugSocket:public KIR4::plug_socket
{
	unsigned
		ID;
	std::wstring
		username = L"*** Unknown ***",
		version = L"*** Unknown ***";
	unsigned long long
		sentbytes = 0,
		recvbytes = 0;
	const Properties
		*properties = NULL;
	char
		*data = NULL;
	unsigned long
		remainsize = 0,
		totalsize = 0;
	Decode
		*encoder = NULL;
	inline std::wstring GetNextFilename() const
	{
		return properties->GetNextFilename(properties->GetDirectory(username, ORIGINAL));
	}
	inline std::wstring GetFileLink(const std::wstring &filename) const
	{
		return properties->GetImgsLink() + username + L"\\" + ORIGINAL + L"\\" + filename;
	}
public:
	PlugSocket()
	{

	}
	void clear()
	{
		if (data)
			delete[] data; data = NULL;
		if (encoder)
			delete encoder; encoder = NULL;
		remainsize = 0;
		totalsize = 0;
	}
	void create(unsigned long size)
	{
		remainsize = size;
		totalsize = size;
		data = new char[size];
	}
	~PlugSocket()
	{
		clear();
	}
	inline double GetStatus() const
	{
		return 1 - (double(remainsize) / double(totalsize));
	}
	void SetUser(const wchar_t *username, const wchar_t *version)
	{
		this->username = username;
		this->version = version;
	}
	void Set(unsigned ID, const Properties *properties)
	{
		this->ID = ID;
		this->properties = properties;
	}
	inline unsigned long long GetSentBytes()
	{
		sentbytes += get_send_bytes();
		return sentbytes;
	}
	inline unsigned long long GetReceivedBytes()
	{
		recvbytes += get_recv_bytes();
		return recvbytes;
	}
	inline unsigned GetID() const
	{
		return ID;
	}
	inline const std::wstring &GetUsername() const
	{
		return username;
	}
	void Print()
	{
		clog.color(KIR4::YELLOW) << GetID();
		(clog.color(KIR4::LYELLOW) << "   Username: ").color(KIR4::YELLOW) << username;
		(clog.color(KIR4::LYELLOW) << "   Version: ").color(KIR4::YELLOW) << version << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Connection status: ").color(KIR4::YELLOW) << get_status() << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "IP: ").color(KIR4::YELLOW) << get_ip_port() << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "send run: ").color(KIR4::YELLOW) << !is_send_done();
		(clog.color(KIR4::LYELLOW) << "    recv run: ").color(KIR4::YELLOW) << !is_recv_done() << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Connected at ").color(KIR4::YELLOW) << get_started_time() << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Bytes sent: ").color(KIR4::YELLOW) << get_send_bytes() << KIR4::eol;
		(clog.color(KIR4::LYELLOW) << "Bytes received: ").color(KIR4::YELLOW) << get_recv_bytes() << KIR4::eol;
		clog.color(KIR4::LPURPLE) << "Completed: ";
		clog.color(KIR4::PURPLE) << GetStatus() * 100;
		clog.color(KIR4::LPURPLE) << " %" << KIR4::eol;
		if (encoder)
			encoder->Print();
		else
			clog.color(KIR4::LYELLOW) << "No encoder added yet.";
		clog << "total received: " << number << KIR4::eol;
		clog << KIR4::eol;
	}
	bool CheckFileStrucure()
	{
		std::wstring
			original = properties->GetDirectory(username, ORIGINAL),
			resized = properties->GetDirectory(username, RESIZED);

		if (!properties->CheckDirectory(original.c_str()))
		{
			(clog.color(KIR4::LRED) << "Directory is not exist: ").color(KIR4::RED) << original << KIR4::eol;
			return false;
		}
		else
			(clog.color(KIR4::LGREEN) << "Directory is OK: ").color(KIR4::GREEN) << original << KIR4::eol;

		if (!properties->CheckDirectory(resized.c_str()))
		{
			(clog.color(KIR4::LRED) << "Directory is not exist: ").color(KIR4::RED) << resized << KIR4::eol;
			return false;
		}
		else
			(clog.color(KIR4::LGREEN) << "Directory is OK: ").color(KIR4::GREEN) << resized << KIR4::eol;
		return true;
	}
	void RecvFormat()
	{
		auto
			it = receive_begin();
		if (it != receive_end())
		{
			do
			{
				if ((*it)->get_code() != KIR4_SOCKET_CODE_USED)
				{
					switch ((*it)->get_code())
					{
						case CodeFormatRGB:
						{
							(clog.color(KIR4::YELLOW) << ID).color(KIR4::LYELLOW) << ". [RECV] CodeFormatRGB" << KIR4::eol;
							if (!encoder)
							{
								clear();
								unsigned
									width,
									height;

								width = (*it)->get_int32();
								height = (*it)->get_int32();
								if (width > 0 && height > 0)
								{
									create(width*height * KIR4::gdi_bitmap::lock_BGR().pixel_bytes());
									encoder = new DecodeFormatRGB(width, height);
									send_message(new KIR4::socket_message_dynamic(CodeFormatAccepted));
									(clog.color(KIR4::YELLOW) << ID).color(KIR4::LYELLOW) << ". [SEND] CodeFormatAccepted" << KIR4::eol;
									set_status(PLUG_SOCKET_STATUS_WAIT_FOR_DATA);
								}
								else
								{
									(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". ";
									(((clog.color(KIR4::LRED) << "Invalid Bitmap size: ").color(KIR4::RED) << width).color(KIR4::LRED) << " * ").color(KIR4::RED) << height << KIR4::eol;
									send_message(new KIR4::socket_message_dynamic(CodeFormatRefused));
									(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". [SEND] CodeFormatRefused" << KIR4::eol;
								}
							}
							else
								(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". Decoder is created yet." << KIR4::eol;
							(*it)->proccessed();
							break;
						}
						case CodeFormatRGBA:
						{
							(clog.color(KIR4::YELLOW) << ID).color(KIR4::LYELLOW) << ". [RECV] CodeFormatRGBA" << KIR4::eol;
							if (!encoder)
							{
								clear();
								unsigned
									width,
									height;

								width = (*it)->get_int32();
								height = (*it)->get_int32();
								if (width > 0 && height > 0)
								{
									create(width*height * KIR4::gdi_bitmap::lock_BGRA().pixel_bytes());
									encoder = new DecodeFormatRGBA(width, height);
									send_message(new KIR4::socket_message_dynamic(CodeFormatAccepted));
									(clog.color(KIR4::YELLOW) << ID).color(KIR4::LYELLOW) << ". [SEND] CodeFormatAccepted" << KIR4::eol;
									set_status(PLUG_SOCKET_STATUS_WAIT_FOR_DATA);
								}
								else
								{
									(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". ";
									(((clog.color(KIR4::LRED) << "Invalid Bitmap size: ").color(KIR4::RED) << width).color(KIR4::LRED) << " * ").color(KIR4::RED) << height << KIR4::eol;
									send_message(new KIR4::socket_message_dynamic(CodeFormatRefused));
									(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". [SEND] CodeFormatRefused" << KIR4::eol;
								}
							}
							else
								(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". Decoder is created yet." << KIR4::eol;
							(*it)->proccessed();
							break;
						}
						case CodeFormatPNG:
						{
							(clog.color(KIR4::YELLOW) << ID).color(KIR4::LYELLOW) << ". [RECV] CodeFormatPNG" << KIR4::eol;
							if (!encoder)
							{
								clear();
								unsigned
									size;

								size = (unsigned)(*it)->get_int64();
								if (size > 0)
								{
									create(size);
									encoder = new DecodeFormatPNG(size);
									send_message(new KIR4::socket_message_dynamic(CodeFormatAccepted));
									(clog.color(KIR4::YELLOW) << ID).color(KIR4::LYELLOW) << ". [SEND] CodeFormatAccepted" << KIR4::eol;
									set_status(PLUG_SOCKET_STATUS_WAIT_FOR_DATA);
								}
								else
								{
									(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". ";
									clog << KIR4::LRED << "Invalid Bitmap size (bytes): " << KIR4::RED << size << KIR4::eol;
									send_message(new KIR4::socket_message_dynamic(CodeFormatRefused));
									(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". [SEND] CodeFormatRefused" << KIR4::eol;
								}
							}
							else
								(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". Decoder is created yet." << KIR4::eol;
							(*it)->proccessed();
							break;
						}
						case CodeFormatJPG:
						{
							(clog.color(KIR4::YELLOW) << ID).color(KIR4::LYELLOW) << ". [RECV] CodeFormatJPG" << KIR4::eol;
							if (!encoder)
							{
								clear();
								unsigned
									size;

								size = (unsigned)(*it)->get_int64();
								if (size > 0)
								{
									create(size);
									encoder = new DecodeFormatJPG(size);
									send_message(new KIR4::socket_message_dynamic(CodeFormatAccepted));
									(clog.color(KIR4::YELLOW) << ID).color(KIR4::LYELLOW) << ". [SEND] CodeFormatAccepted" << KIR4::eol;
									set_status(PLUG_SOCKET_STATUS_WAIT_FOR_DATA);
								}
								else
								{
									(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". ";
									clog << KIR4::LRED << "Invalid Bitmap size (bytes): " << KIR4::RED << size << KIR4::eol;
									send_message(new KIR4::socket_message_dynamic(CodeFormatRefused));
									(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". [SEND] CodeFormatRefused" << KIR4::eol;
								}
							}
							else
								(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". Decoder is created yet." << KIR4::eol;
							(*it)->proccessed();
							break;
						}
					}
				}
				it++;
			}
			while (it != receive_end());
			recv_message_collector();
		}
	}
	unsigned long
		number = 0;

	void RecvData()
	{
		auto
			it = receive_begin();
		if (it != receive_end())
		{
			do
			{
				if ((*it)->get_code() != KIR4_SOCKET_CODE_USED)
				{
					switch ((*it)->get_code())
					{
						case CodeData:
						{
							number++;
							//(clog.color(YELLOW) << ID).color(KIR4::LYELLOW) << ". [RECV] CodeData" << KIR4::eol;
							if (encoder)
							{
								unsigned long
									pos = unsigned long((*it)->get_int64());
								unsigned long
									size = (*it)->get_remain_size();
								//clog << size << " <= " << remainsize << KIR4::eol;
								if (size <= remainsize)
								{
									CopyMemory(&data[pos], (*it)->get_data() + 8, size);
									remainsize -= size;

									if (remainsize == 0)
									{
										(clog.color(KIR4::GREEN) << ID).color(KIR4::LGREEN) << ". Data receive completed." << KIR4::eol;
										(clog.color(KIR4::YELLOW) << ID).color(KIR4::LYELLOW) << ". Start saving." << KIR4::eol;
										std::wstring
											filename = GetNextFilename();
										if (
											encoder->Save(data, properties->GetDirectory(username, ORIGINAL) + filename,
												properties->GetDirectory(username, RESIZED) + filename,
												properties->GetResizeWidth(),
												properties->GetResizeHeight(),
												properties->GetResizeColor()
											)
											)
											((clog.color(KIR4::GREEN) << ID).color(KIR4::LGREEN) << ". Data save completed: ").color(KIR4::GREEN) << filename << KIR4::eol;
										else
											(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". Data can not be saved." << KIR4::eol;
										KIR4::socket_message
											*message = new KIR4::socket_message_dynamic(CodeEndOfData);
										clog << KIR4::LAQUA << "Direct link: " << KIR4::AQUA << GetFileLink(filename).c_str() << KIR4::eol;
										message->set_wstr(GetFileLink(filename).c_str());
										send_message(message);
										clear();

										set_status(PLUG_SOCKET_STATUS_WAIT_FOR_FORMAT);
									}
								}
								else
								{
									KIR4::socket_message
										*message = new KIR4::socket_message_dynamic(CodeDataError);
									send_message(message);
									set_status(PLUG_SOCKET_STATUS_WAIT_FOR_FORMAT);
								}
							}
							else
								(clog.color(KIR4::RED) << ID).color(KIR4::LRED) << ". Decoder is not created yet." << KIR4::eol;
							(*it)->proccessed();
							break;
						}
					}
				}
				it++;
			}
			while (it != receive_end());
			recv_message_collector();
		}
	}
	inline void SetProperties(const Properties *properties)
	{
		this->properties = properties;
	}
};