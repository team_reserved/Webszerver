#define WIN32_LEAN_AND_MEAN

//#include <KIR\KIR4_debug.h>
//#define KIR4_SOCKET_LOG

const wchar_t PROGRAM_VERSION[] = L"2.1.0.8";

const wchar_t *COMPATIBLE_VERSIONS[] = {L"1.1.0.0 - EZ",L"1.1.0.1 - EZ",L"2.0.0.1A - EZ",L"2.0.0.3A - EZ"};

#include <KIR\KIR4_event.h>
#include <KIR\KIR4_console.h>
#include <KIR\KIR4_serverA.h>
#include <KIR\KIR4_string.h>

#include "MainEvent.h"

int main(int, char *[])
{
	KIR4::gdi_bitmap::initialize();

	clog.set_name("UYIS");

	ServerEvent
		event;

	event.run();

	KIR4::gdi_bitmap::shut_down();
	clog.pause();
	return 0;
}