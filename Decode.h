#pragma once

class Decode
{
public:
	Decode()
	{
	}
	virtual ~Decode()
	{
	}
	bool SaveBitmap(KIR4::gdi_bitmap &bitmap, const std::wstring &original, const std::wstring &resized, int w, int h, Gdiplus::Color bcolor, const wchar_t *format = L"image/png")
	{
		if (bitmap.save(original.c_str(), format))
			clog << KIR4::GREEN << original << KIR4::eol;
		else
		{
			clog << KIR4::RED << original << KIR4::eol;
			return false;
		}

		bitmap.frame_resize(w, h);
		KIR4::gdi_bitmap
			mini = new Gdiplus::Bitmap(w, h);
		mini.clear_to_color(Gdiplus::Color(0, 0, 0, 0));
		bitmap.draw(float((w - bitmap.width()) / 2), float((h - bitmap.height()) / 2), Gdiplus::Graphics(mini));

		if (mini.save(resized.c_str()))
			clog << KIR4::GREEN << resized << KIR4::eol;
		else
		{
			DeleteFileW(original.c_str());
			clog << KIR4::RED << resized << KIR4::eol;
			return false;
		}

		return true;
	}
	/*true visszatérés ha minden adat megérkezett*/
	virtual bool Save(const char*, const std::wstring &original, const std::wstring &resized, int rwidth, int rheight, Gdiplus::Color bcolor) = 0;
	virtual void Print() = 0;
};